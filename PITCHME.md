## Linux .Net Core Containers In Kubernetes


---

### Pitch
Let's run .Net code 
- On Linux
- Orchestrated in Kubernetes
I created a sample project as part of an Implementation Guide for how we can run .Net Core on Linux and wanted to share my findings with a larger group.


---

### Sample Project
- Created to prototype and prove out concepts
- Contains a sample API, a library is consumes & a unit test project.
- Runs against containerized SQL Server

--- 
### CI/CD
- Local Development - no docker until the test environment (unless you want to)
- Demo building container if possible

--- 
### PlatformSDK
![The Elephant In The Room](http://www.bumpbabyandyou.co.uk/wp-content/uploads/2018/02/elephant.jpg)

--- 

### Consuming Configuration
Application config is in a few separate files. Secrets in a separate file. This is not built on the container and provided at runtime

---
### Tracking ID

---
### Dependency Injection

---
### Logging
	File based 

	Rotation & Retention

	Log Types
		Diagnostic Logs
		Web Access Logs

---
### Kestrel

---
### Monitoring

---
### Dependency Injection
	Castle Windsor

---
### Raising Events 

---
